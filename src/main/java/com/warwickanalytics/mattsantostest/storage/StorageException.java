package com.warwickanalytics.mattsantostest.storage;

/**
 * Created by matheussantos on 19/06/17.
  */
public class StorageException extends  RuntimeException {

    public StorageException(String message) {
        super(message);
    }

    public StorageException(String message, Throwable cause) {
        super(message, cause);
    }

}
