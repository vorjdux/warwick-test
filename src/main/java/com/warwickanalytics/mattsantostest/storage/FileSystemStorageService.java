package com.warwickanalytics.mattsantostest.storage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.stream.Stream;

/**
 * Created by matheussantos on 19/06/17.
 */
@Service
public class FileSystemStorageService implements StorageService {

    private final Path rootLocation;

    @Autowired
    public FileSystemStorageService(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
    }

    @Override
    public void store(MultipartFile file) {
        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + file.getOriginalFilename());
            }
            Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new StorageException("Failed to store file " + file.getOriginalFilename(), e);
        }
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.rootLocation, 1)
                    .filter(path -> !path.equals(this.rootLocation))
                    .map(this.rootLocation::relativize);
        } catch (IOException e) {
            throw new StorageException("Failed to read stored files", e);
        }

    }

    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new StorageFileNotFoundException("Could not read file: " + filename);

            }
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    private List<String> loadResourceAsLines(Resource resource) {
        try {
            return Files.readAllLines(resource.getFile().toPath());
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    public ResourceResult loadResourceAsList(Resource resource) {
        ResourceResult resourceResult = new ResourceResult();
        List<Map<String, Object>> tmpList = new ArrayList<>();
        try {
            for (String line : loadResourceAsLines(resource)) {
                if (resourceResult.getAllColumns()  == null) {
                    resourceResult.setAllColumns(Arrays.asList(line.split(",")));
                } else {
                    String[] values = line.split(",");

                    Map<String, Object> dict = new HashMap<>();

                    for (int i = 0; i < resourceResult.getAllColumns().size(); i++) {
                        dict.put(resourceResult.getAllColumns().get(i), values[i]);
                    }

                    tmpList.add(dict);
                }
            }
        } catch (Exception e) {
            resourceResult.setAllColumns(Collections.emptyList());
        }
        resourceResult.setList(tmpList);
        return resourceResult;
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    @Override
    public void init() {
        try {
            Files.createDirectory(rootLocation);
        } catch (IOException e) {
            throw new StorageException("Could not initialize storage", e);
        }
    }

}
