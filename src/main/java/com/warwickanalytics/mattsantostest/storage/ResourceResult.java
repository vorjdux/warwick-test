package com.warwickanalytics.mattsantostest.storage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by matheussantos on 21/06/17.
 */
public class ResourceResult {

    private List<Map<String, Object>> list;
    private List<String> allColumns;

    private Integer fMin;
    private Integer fMmax;

    public List<Map<String, Object>> getList() {
        return list;
    }

    public void setList(List<Map<String, Object>> list) {
        calculateMinMax(list);
        List<Map<String, Object>> newList = new ArrayList<>();
        for (Map<String, Object> line : list) {
            if (line.get("Decision").equals("0")) {
                Boolean insert = true;
                for (String column : getDynamicColumns()) {
                    Integer value = Integer.parseInt((String) line.get(column));
                    if (value < fMin || value > fMmax) {
                        insert = false;
                        break;
                    }
                }
                if (insert) {
                    newList.add(line);
                }
            } else {
                newList.add(line);
            }
        }
        this.list = newList;
    }

    public List<String> getAllColumns() {
        return allColumns;
    }

    public void setAllColumns(List<String> allColumns) {
        this.allColumns = allColumns;
    }

    public List<String> getDynamicColumns() {
        List<String> tmpColumns = new ArrayList<>();
        for (String column : getAllColumns()) {
            if (!column.equals("Id") && !column.equals("Decision")) {
                tmpColumns.add(column);
            }
        }
        return tmpColumns;
    }

    private void calculateMinMax(List<Map<String, Object>> list) {
        for (Map<String, Object> line : list) {
            if (line.get("Decision").equals("1")) {
                for (String column : getDynamicColumns()) {
                    Integer value = Integer.parseInt((String)line.get(column));
                    if (fMmax == null || fMin == null) {
                        fMin = value;
                        fMmax = value;
                    } else {
                        if (value < fMin) fMin = value;
                        if (value > fMmax) fMmax = value;
                    }
                }
            }
        }
    }

}
