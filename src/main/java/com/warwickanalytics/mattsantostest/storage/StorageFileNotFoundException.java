package com.warwickanalytics.mattsantostest.storage;

/**
 * Created by matheussantos on 19/06/17.
 */
public class StorageFileNotFoundException extends StorageException {

    public StorageFileNotFoundException(String message) {
        super(message);
    }

    public StorageFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

}
