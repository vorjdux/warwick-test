package com.warwickanalytics.mattsantostest.controller;

import com.warwickanalytics.mattsantostest.storage.ResourceResult;
import com.warwickanalytics.mattsantostest.storage.StorageFileNotFoundException;
import com.warwickanalytics.mattsantostest.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by matheussantos on 19/06/17.
 */
@Controller
public class FileUploadController {

    private final StorageService storageService;

    @Autowired
    public FileUploadController(StorageService storageService) {
        this.storageService = storageService;
    }

    @GetMapping("/")
    public String index(Model model) {

        if (model.containsAttribute("file")) {
            Map<String, Object> fields = model.asMap();
            Resource resource = storageService.loadAsResource((String)fields.get("file"));

            ResourceResult resourceResult = storageService.loadResourceAsList(resource);
            if (!resourceResult.getList().isEmpty()) {
                model.addAttribute("lines", resourceResult.getList());
                model.addAttribute("dynamicColumns", resourceResult.getDynamicColumns());
            } else {
                model.addAttribute("lines", Collections.emptyList());
                model.addAttribute("dynamicColumns", new String[]{});
            }
        } else {
            model.addAttribute("lines", Collections.emptyList());
        }

        return "index";
    }

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

    @PostMapping("/")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) {

        storageService.store(file);
        redirectAttributes.addFlashAttribute("message",
                "You successfully uploaded " + file.getOriginalFilename() + "!");

        redirectAttributes.addFlashAttribute("file", file.getOriginalFilename());

        return "redirect:/";
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

}
